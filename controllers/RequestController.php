<?php

namespace app\controllers;

use app\services\VarDumper;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class RequestController extends Controller
{
    public $enableCsrfValidation = false;

    public function init()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $key = Yii::$app->request->post('key');
        if ($key != Yii::$app->params['key']) {
            throw new ForbiddenHttpException('Запрещено, код не верный');
        }
    }

    /**
     *
     * @return string
     * @throws
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->post();
        unset($params['key']);

        return $this->send($params);
    }

    /**
     * Выводит монеты на нужный кошелек
     * REQUEST
     * - amountNQT - int - atom
     * - recipient - string
     * - recipientPublicKey - string
     * - comment - string
     *
     * @return string
     * @throws
     */
    public function actionWithdraw()
    {
        $PRIZM_URL = \Yii::$app->params['PRIZM_URL'];
        $params = Yii::$app->request->post();
        $path = 'prizm';

        $params = [
            'requestType'        => 'sendMoney',
            'amountNQT'          => $params['amountNQT'],
            'recipient'          => $params['recipient'],
            'recipientPublicKey' => $params['recipientPublicKey'],
            'publicKey'          => \Yii::$app->params['PRIZM_PUBLIC_KEY'],
            'deadline'           => 60,
            'secretPhrase'       => \Yii::$app->params['PRIZM_PRIVATE'],
        ];

//        $rows2 = [];
//        foreach ($params as $k => $v) {
//            $rows2[] = $k . '=' . urlencode($v);
//        }
//
//        $c = new Client(['baseUrl' => $PRIZM_URL . "/" . $path]);
//        $response = $c
//            ->post($params)
//            ->setOptions(['sslVerifyPeer' => false])
//            ->send();
//
//
//        $data = Json::decode($response->content);

        $array = $params;

        $ch = curl_init($PRIZM_URL . "/" . $path);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($array, '', '&'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $html = curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
        }

        curl_close($ch);

        if (isset($error_msg)) {
            Yii::error($error_msg);
            return ['error' => $error_msg];
        }

        $data = Json::decode($html);

        return $data;
    }

    /**
     * @param string $summa         сумма для перевода
     * @param string $pzm           Кошелек для вывода
     * @param string $public_key    Публичный ключ
     * @param string $text          Коментарий
     */
    private function payPZM($summa, $pzm, $public_key, $text)
    {
        $p2 = \Yii::$app->params['PRIZM_SENDKEY'];   //  это пароль вы который указывали при настройке сервлета
        $params = [
            'sendkey'     => $p2,
            'amount'      => $summa,
            'comment'     => urlencode($text),
            'destination' => $pzm,
            'publickey'   => $public_key,
        ];

        return $this->send($params, 'send');
    }

    private function send($params, $path = 'prizm')
    {
        $arrContextOptions = [
            "ssl" => [
                "verify_peer"      => false,
                "verify_peer_name" => false,
            ],
        ];

        $rows = [];
        foreach ($params as $k => $v) {
            $rows[] = $k . '=' . urlencode($v);
        }

        $PRIZM_URL = \Yii::$app->params['PRIZM_URL'];

        $response = file_get_contents($PRIZM_URL . "/" . $path . "?" . join('&', $rows), false, stream_context_create($arrContextOptions));

        try {
            $data = Json::decode($response);
        } catch (\Exception $e) {
            throw $e;
        }

        return $data;
    }

}
