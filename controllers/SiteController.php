<?php

namespace app\controllers;

use app\services\VarDumper;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return '';
        $path = 'prizm';
        $arrContextOptions = [
            "ssl" => [
                "verify_peer"      => false,
                "verify_peer_name" => false,
            ],
        ];

        $params = [
            'sendkey'     => \Yii::$app->params['PRIZM_SENDKEY'],
            'amount'      => 0.01,
            'comment'     => '',
            'destination' => 'PRIZM-PP8C-HY3J-G5VU-PMQJV',
            'publickey'   => \Yii::$app->params['PRIZM_PUBLIC_KEY'],
        ];

        $params = [
            'requestType'        => 'sendMoney',
            'amountNQT'          => 1,
            'recipient'          => 'PRIZM-PP8C-HY3J-G5VU-PMQJV',
            'recipientPublicKey' => '33cdbaa641435ac841e8118e3500d98265b6549ff2a23fbde1a468cbdcf1ed62',
            'publicKey'          => \Yii::$app->params['PRIZM_PUBLIC_KEY'],
            'deadline'           => 32,
            'secretPhrase'       => \Yii::$app->params['PRIZM_PRIVATE'],
        ];

        $rows2 = [];
        foreach ($params as $k => $v) {
            $rows2[] = $k . '=' . urlencode($v);
        }
        $url = "http://localhost:9976/" . $path . "?" . join('&', $rows2); // Connection refused

        $c = new Client(['baseUrl' => "http://localhost:9976/" . $path]);
        $response = $c->post($params)->send();

        VarDumper::dump($response);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionPrizm777()
    {
        $client = new Client(['baseUrl' => 'http://5.187.3.161:7742']);

        $response = $client->get('prizm', Yii::$app->request->get())->send();

        try {
            $data = Json::decode($response->content);
            VarDumper::dump($data);
        } catch (\Exception $e) {
            VarDumper::dump($response);
        }


    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionPrizm778()
    {
        $client = new Client(['baseUrl' => 'http://webserver:7742']);

        $response = $client->get('prizm', Yii::$app->request->get())->send();

        try {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $data = Json::decode($response->content);
            return $data;
        } catch (\Exception $e) {
            VarDumper::dump($response);
        }


    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionPrizm998()
    {
        $client = new Client(['baseUrl' => 'http://webserver:7742']);

        $response = $client->get('prizm', [
            'requestType' => 'getBlockchainTransactions',
            'account' => 'PRIZM-GPN2-8CZ7-PNYP-8CEHG',
            'lastIndex' => 10,
        ])->send();

        try {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $data = Json::decode($response->content);
            return $data;
        } catch (\Exception $e) {
            VarDumper::dump($response);
        }


    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionPrizm999()
    {
        $client = new Client(['baseUrl' => 'http://localhost:9976']) ;

        $response = $client->get('prizm', [
            'requestType' => 'getBlockchainTransactions',
            'account' => 'PRIZM-GPN2-8CZ7-PNYP-8CEHG',
            'lastIndex' => 10,
        ])->send();

        try {
            $data = Json::decode($response->content);
            VarDumper::dump($data);
        } catch (\Exception $e) {
            VarDumper::dump($response);
        }


    }

    /**
     * Displays homepage.
     *
     * @return string
     * @throws
     */
    public function actionIndex2()
    {
        $arrContextOptions = [
            "ssl" => [
                "verify_peer"      => false,
                "verify_peer_name" => false,
            ],
        ];

        $response = file_get_contents("https://prizm-api.neiro-n.com:9976/prizm?requestType=getBlockchainStatus", false, stream_context_create($arrContextOptions));



        VarDumper::dump($response);


        return  Json::decode($response->content);
    }

}
