<?php

return [
    'adminEmail'       => 'admin@example.com',
    'senderEmail'      => 'noreply@example.com',
    'senderName'       => 'Example.com mailer',
    'key'              => env('PRIZM_API_KEY'),
    'PRIZM_SENDKEY'    => env('PRIZM_SENDKEY'),
    'PRIZM_PUBLIC_KEY' => env('PRIZM_PUBLIC_KEY'),
    'PRIZM_PRIVATE'    => env('PRIZM_PRIVATE'),
    'PRIZM_URL'        => env('PRIZM_URL'),
];
